package domain;

import java.util.ArrayList;
import java.util.List;

public class Company {
    private List<Employee> employees;

    public Company(){
        employees = new ArrayList<>();
    }

    public void startWork() {
        employees.forEach(e -> e.work());
    }

    public void addEmployee(Employee employee){
        employees.add(employee);
    }



}
