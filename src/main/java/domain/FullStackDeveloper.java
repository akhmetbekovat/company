package domain;

public class FullStackDeveloper extends Worker implements BackEndDeveloper, FrontEndDeveloper {

    public FullStackDeveloper(String name) {
        super(name);
    }

    @Override
    public void writeProgram() {
        System.out.println("I`m writing c++ application");
    }

    @Override
    public void writeFront() {
        System.out.println("I`m creating an interface");
    }

    @Override
    public void develop() {
        System.out.println("My name is "+ getName());
        writeFront();
        writeProgram();
    }

    @Override
    public void work() {
        develop();
    }
}
