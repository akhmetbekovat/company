package domain;

public class JavaDeveloper extends Worker implements BackEndDeveloper {

    public JavaDeveloper(String name) {
        super(name);
    }
    private void writeJava() {
        System.out.println("Hello, I`m " + getName() +" and I`m writing Java..");
    }

    @Override
    public void writeProgram() {
        writeJava();
    }

    @Override
    public void develop() {
        writeProgram();
    }

    @Override
    public void work() {
        develop();
    }
}
