package domain;

public interface BackEndDeveloper extends Developer{

    void writeProgram();
}
