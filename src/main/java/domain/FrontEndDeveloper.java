package domain;

public interface FrontEndDeveloper extends Developer {
    void writeFront();
}
