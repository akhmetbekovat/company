package domain;

public class JavaScriptDev extends Worker implements FrontEndDeveloper {

    public JavaScriptDev(String name){
        super(name);
    }
    private void writeJS() {
        System.out.println("Hello I`m "+getName()+" I`m writing Javascript");
    }

    @Override
    public void writeFront() {
        writeJS();
    }

    @Override
    public void develop() {
        writeFront();
    }

    @Override
    public void work() {
        develop();
    }
}
